package br.ifsc.edu.imigrafaq;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

public class AddTopico extends AppCompatActivity {

    EditText Pergunta, Resposta;
    Button Enviar;

    DatabaseReference databaseCategoria;

    ListView listView;

    List<Topico> enviadoList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_topico);

        databaseCategoria = FirebaseDatabase.getInstance().getReference("Categoria");

        Pergunta = findViewById(R.id.Pergunta);
        Resposta = findViewById(R.id.Resposta);
        Enviar = findViewById(R.id.Enviar);

        listView = (ListView) findViewById(R.id.listView);

        enviadoList = new ArrayList<>();
        Enviar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                enviar();
            }
        });
    }

    @Override
    protected void onStart() {
        super.onStart();

        databaseCategoria.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                enviadoList.clear();

                for (DataSnapshot categoriaSnapshot : dataSnapshot.getChildren()){
                    Topico enviado = categoriaSnapshot.getValue(Topico.class);

                    enviadoList.add(enviado);
                }

                Adapter adapter = new (AddTopico.this, enviadoList);
                Adapter.setAdapter(adapter);

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    private void enviar(){
        String pergunta = Pergunta.getText().toString().trim();
        String resposta = Resposta.getText().toString().trim();



        if(!TextUtils.isEmpty(pergunta) || !TextUtils.isEmpty(resposta)){

            String id = databaseCategoria.push().getKey();

            Topico perguntaTitulo = new Topico(id, pergunta, resposta);

            databaseCategoria.child(id).setValue(perguntaTitulo);

            Toast.makeText(this, "Pergunta submetida", Toast.LENGTH_LONG).show();
        }else{
            Toast.makeText(this, "Não deixe este campo em branco", Toast.LENGTH_LONG).show();
        }

    }
}